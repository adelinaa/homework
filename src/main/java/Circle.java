public class Circle extends Shape{
    private float lengthOfSide;

    public Circle(String name, int numberOfSides, int numberOfAngles, float lengthOfSide){
        super(name, numberOfSides, numberOfAngles);
        this.lengthOfSide = lengthOfSide;
    }

    private float getLengthOfSide(){
        return this.lengthOfSide;
    }

    private void setLengthOfSide(float lengthOfSide){
        this.lengthOfSide = lengthOfSide;
    }
}
