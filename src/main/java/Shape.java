public class Shape {
    private String name;
    private int numberOfSides;
    private int numberOfAngles;

    public Shape(String name, int numberOfSides, int numberOfAngles){
        this.name = name;
        this.numberOfSides = numberOfSides;
        this.numberOfAngles = numberOfAngles;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getNumberOfSides(){
        return numberOfSides;
    }

    public void setNumberOfSides(int numberOfSides){
        this.numberOfSides = numberOfSides;
    }

    public int getNumberOfAngles(){
        return numberOfAngles;
    }

    public void setNumberOfAngles(int numberOfAngles){
        this.numberOfAngles = numberOfAngles;
    }


}
