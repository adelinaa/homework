public class Rectangle extends Shape {
    private float radius;

    public Rectangle(String name, int numberOfSides, int numberOfAngles, float radius) {
        super(name, numberOfSides, numberOfAngles);
        this.radius = radius;
    }

    private float getRadius(){
        return this.radius;
    }

    private void setRadius(float radius){
        this.radius = radius;
    }


}
